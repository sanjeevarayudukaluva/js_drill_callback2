function dataBasedOnBoardId(data, id, callback) {
  setTimeout(() => {
    const listsData = data[id];

    callback(listsData);
  }, 2000);
}
module.exports = dataBasedOnBoardId;
