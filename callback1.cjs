function boardInformation(board, id, callback) {
  setTimeout(() => {
    const boardBasedOnId = board.filter((eachBoard) => id === eachBoard.id);
    callback(boardBasedOnId);
  }, 2 * 1000);
}
module.exports = boardInformation;
