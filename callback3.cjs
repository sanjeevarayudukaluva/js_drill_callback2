function cardsDetailsBasedOnListsId(data, id, callback) {
  setTimeout(() => {
    const cardsDetails = data[id];
    callback(cardsDetails);
  }, 2000);
}
module.exports = cardsDetailsBasedOnListsId;
